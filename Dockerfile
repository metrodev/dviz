FROM node:alpine

WORKDIR /usr/local/dviz

COPY package.json .
COPY *.lock .

RUN yarn install

COPY . .

RUN yarn build