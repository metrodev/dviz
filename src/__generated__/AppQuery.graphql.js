/**
 * @flow
 * @relayHash 9e7e9cd6046ca30328f87d8c38dec444
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type AppQueryVariables = {||};
export type AppQueryResponse = {|
  +btc: ?$ReadOnlyArray<?{|
    +price: ?number,
    +date: ?string,
    +id: ?string,
    +source: ?string,
  |}>,
  +bcv: ?$ReadOnlyArray<?{|
    +price: ?number,
    +date: ?string,
    +id: ?string,
    +source: ?string,
  |}>,
  +dt: ?$ReadOnlyArray<?{|
    +price: ?number,
    +date: ?string,
    +id: ?string,
    +source: ?string,
  |}>,
  +atm: ?$ReadOnlyArray<?{|
    +price: ?number,
    +date: ?string,
    +id: ?string,
    +source: ?string,
  |}>,
|};
export type AppQuery = {|
  variables: AppQueryVariables,
  response: AppQueryResponse,
|};
*/


/*
query AppQuery {
  btc: rates(limit: 500, source: "btc") {
    price
    date
    id
    source
  }
  bcv: rates(limit: 200, source: "bcv") {
    price
    date
    id
    source
  }
  dt: rates(limit: 200, source: "dt") {
    price
    date
    id
    source
  }
  atm: rates(limit: 200, source: "dt") {
    price
    date
    id
    source
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "ScalarField",
    "alias": null,
    "name": "price",
    "args": null,
    "storageKey": null
  },
  {
    "kind": "ScalarField",
    "alias": null,
    "name": "date",
    "args": null,
    "storageKey": null
  },
  {
    "kind": "ScalarField",
    "alias": null,
    "name": "id",
    "args": null,
    "storageKey": null
  },
  {
    "kind": "ScalarField",
    "alias": null,
    "name": "source",
    "args": null,
    "storageKey": null
  }
],
v1 = {
  "kind": "Literal",
  "name": "limit",
  "value": 200
},
v2 = [
  (v1/*: any*/),
  {
    "kind": "Literal",
    "name": "source",
    "value": "dt"
  }
],
v3 = [
  {
    "kind": "LinkedField",
    "alias": "btc",
    "name": "rates",
    "storageKey": "rates(limit:500,source:\"btc\")",
    "args": [
      {
        "kind": "Literal",
        "name": "limit",
        "value": 500
      },
      {
        "kind": "Literal",
        "name": "source",
        "value": "btc"
      }
    ],
    "concreteType": "Rate",
    "plural": true,
    "selections": (v0/*: any*/)
  },
  {
    "kind": "LinkedField",
    "alias": "bcv",
    "name": "rates",
    "storageKey": "rates(limit:200,source:\"bcv\")",
    "args": [
      (v1/*: any*/),
      {
        "kind": "Literal",
        "name": "source",
        "value": "bcv"
      }
    ],
    "concreteType": "Rate",
    "plural": true,
    "selections": (v0/*: any*/)
  },
  {
    "kind": "LinkedField",
    "alias": "dt",
    "name": "rates",
    "storageKey": "rates(limit:200,source:\"dt\")",
    "args": (v2/*: any*/),
    "concreteType": "Rate",
    "plural": true,
    "selections": (v0/*: any*/)
  },
  {
    "kind": "LinkedField",
    "alias": "atm",
    "name": "rates",
    "storageKey": "rates(limit:200,source:\"dt\")",
    "args": (v2/*: any*/),
    "concreteType": "Rate",
    "plural": true,
    "selections": (v0/*: any*/)
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "AppQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": (v3/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "AppQuery",
    "argumentDefinitions": [],
    "selections": (v3/*: any*/)
  },
  "params": {
    "operationKind": "query",
    "name": "AppQuery",
    "id": null,
    "text": "query AppQuery {\n  btc: rates(limit: 500, source: \"btc\") {\n    price\n    date\n    id\n    source\n  }\n  bcv: rates(limit: 200, source: \"bcv\") {\n    price\n    date\n    id\n    source\n  }\n  dt: rates(limit: 200, source: \"dt\") {\n    price\n    date\n    id\n    source\n  }\n  atm: rates(limit: 200, source: \"dt\") {\n    price\n    date\n    id\n    source\n  }\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '6807a2dc15b986e70b1e748e21d179fe';
module.exports = node;
