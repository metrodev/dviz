import React from 'react';
import graphql from 'babel-plugin-relay/macro';
import { Chart, ChartCanvas } from 'react-stockcharts';
import { XAxis, YAxis } from "react-stockcharts/lib/axes";
import { AreaSeries } from "react-stockcharts/lib/series";
import { scaleTime } from "d3-scale";

import { QueryRenderer } from 'react-relay';
import environment from './graphql';

import './App.css';

function App() {
  return (
    <div className="App">
      <h1 className="App__Header">Histórico de Datos</h1>
      <QueryRenderer query={graphql`
        query AppQuery {
          btc: rates(limit: 500, source: "btc") {
            price
            date
            id
            source
          }

          bcv: rates(limit: 200, source: "bcv") {
            price
            date
            id
            source
          }

          dt: rates(limit: 200, source: "dt") {
            price
            date
            id
            source
          }

          atm: rates(limit: 200, source: "dt") {
            price
            date
            id
            source
          }
        }
      `} 
        environment={environment}
        render={({error, props}) => {
          if(!props) {
            return <h1>Cargando...</h1>
          }

        const rates = src => props[src].map(rate => {
          return {
            ...rate,
            date: new Date(rate.date),
          };
        }).reverse();

        const btc = rates('btc');
        const bcv = rates('bcv');
        const atm = rates('atm');
        const dt = rates('dt');

        return (
        <div>
        <h1 >Bitcoin</h1>
        <ChartCanvas width={window.innerWidth} height={400}
            margin={{ left: 50, right: 50, top:10, bottom: 30 }}
            seriesName="LBC/BTC"
            data={btc} type="svg"
            xAccessor={d => d.date} xScale={scaleTime()}
            xRange={[btc[0], btc[btc.length - 1]]}
            >
            <Chart id={0} yExtents={d => d.price}>
                <XAxis axisAt="bottom" orient="bottom" ticks={10}/>
                <YAxis axisAt="left" orient="left" />
                <AreaSeries yAccessor={(d) => d.price}/>
            </Chart>
        </ChartCanvas>
        <h1>BCV</h1>
        <ChartCanvas width={window.innerWidth} height={400}
            margin={{ left: 50, right: 50, top:10, bottom: 30 }}
            seriesName="BCV"
            data={bcv} type="svg"
            xAccessor={d => d.date} xScale={scaleTime()}
            xRange={[bcv[0], bcv[bcv.length - 1]]}
            >
            <Chart id={0} yExtents={d => d.price}>
                <XAxis axisAt="bottom" orient="bottom" ticks={10}/>
                <YAxis axisAt="left" orient="left" />
                <AreaSeries yAccessor={(d) => d.price}/>
            </Chart>
        </ChartCanvas>
        <h1>Airtm</h1>
        <ChartCanvas width={window.innerWidth} height={400}
            margin={{ left: 50, right: 50, top:10, bottom: 30 }}
            seriesName="Airtm"
            data={atm} type="svg"
            xAccessor={d => d.date} xScale={scaleTime()}
            xRange={[atm[0], atm[atm.length - 1]]}
            >
            <Chart id={0} yExtents={d => d.price}>
                <XAxis axisAt="bottom" orient="bottom" ticks={10}/>
                <YAxis axisAt="left" orient="left" />
                <AreaSeries yAccessor={(d) => d.price}/>
            </Chart>
        </ChartCanvas>
        <h1>DolarToday</h1>
        <ChartCanvas width={window.innerWidth} height={400}
            margin={{ left: 50, right: 50, top:10, bottom: 30 }}
            seriesName="DolarToday"
            data={dt} type="svg"
            xAccessor={d => d.date} xScale={scaleTime()}
            xRange={[dt[0], dt[dt.length - 1]]}
            >
            <Chart id={0} yExtents={d => d.price}>
                <XAxis axisAt="bottom" orient="bottom" ticks={10}/>
                <YAxis axisAt="left" orient="left" />
                <AreaSeries yAccessor={(d) => d.price}/>
            </Chart>
        </ChartCanvas>
        </div>
        )
        }}
      />
    </div>
  );
}

export default App;
